
Voraussetzungen
===============


Ihr solltet auf Euren PC/Notebook ein aktuelles `kubectl` installiert haben
(siehe https://kubernetes.io/de/docs/tasks/tools/install-kubectl/ ).

Sowie einen Code-Editor Eurer Wahl der mit YAML-Dateien umgehen kann 
(Visual-Studio-Code, Sublime, Notepad++, ...) 

und natürlich `Git` um das Übungsprojekt clonen zu können.

Das Übungsprojekt  ist public und kann von https://gitlab.com/osp-stb-public/k8s-practice-workshop geclont werden.


Wichtige Hinweise:
* Falls Ihr hinter Firewalls arbeitet.
  Bitte stellt sicher, dass Ihr folgende URL von Eurem System aus erreichen könnt:
  https://c4tqn3dgzsw.eu-frankfurt-1.clusters.oci.oraclecloud.com:6443
* Wenn Ihr unter Windows das Git-Projekt cloned, stellt sicher, 
  dass die Linux-Zeilenumbrüchen (\n) erhalten bleiben.  
  Diese dürfen **NICHT** in die für Windows üblichen **\n\r** gewandelt werden!


