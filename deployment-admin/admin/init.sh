#!/bin/bash
# Admin-Stuff to prepare work in K8s
set -e

script_dir=`dirname "$0"`
cd ${script_dir}


export NAMESPACE=k8s-workshop

# create namespace
cat namespace.yaml | envsubst | kubectl apply -f -

# create PersistVolume ReadWriteMany
kubectl apply -f ws-data-pv.yaml

# create deployment account
kubectl apply -f deploy-role.yaml

# get kubeconfig
./k8s-service-account-kubeconfig.sh $NAMESPACE deployment-account

echo "Use new kubeconfig and switch namespace to $NAMESPACE"
export KUBECONFIG=$PWD/kubeconfig
kubectl config set-context --current --namespace=$NAMESPACE

kubectl apply -f ws-data-pvc.yaml
kubectl apply -f job-to-init-data.yaml 
