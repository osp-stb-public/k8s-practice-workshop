#!/usr/bin/env sh
# use it with bledig/alpine-psqlclient:3
#
# This script need follow defined environment variables:
# - PGHOST
# - DB_NAME
# - BACKUP_URL
# - BACKUP_PREFIX  (optional)
#
set -e

script_dir=`dirname "$0"`
cd ${script_dir}

if [ -z "$PGHOST" ]; then
  echo "Missing env PGHOST"
  exit 1
fi
if [ -z "$DB_NAME" ]; then
  echo "Missing env DB_NAME"
  exit 1
fi
if [ -z "$BACKUP_URL" ]; then
  echo "Missing env BACKUP_URL"
  exit 1
fi

backupdir=/backups
d=$(date --utc +'%d')
backupfile=${BACKUP_PREFIX}${DB_NAME}.db.${d}.backup
backup_path=$backupdir/$backupfile

OCI_BUCKET_URL=$BACKUP_URL

mkdir -p $backupdir

echo "Start Backup of $DB_NAME to $backup_path ..."
pg_dump -w -U postgres --format custom --blobs $DB_NAME > $backup_path
echo "Finished Backup of $DB_NAME ."
ls -lh $backup_path

# Transfer Backup to OCI-Object-Store
echo "Transfer $backup_path to $OCI_BUCKET_URL"
curl --upload-file $backup_path $OCI_BUCKET_URL
echo "Transfer $backup_path finished."
